<!-- https://viblo.asia/p/validate-du-lieu-form-don-gian-hieu-qua-hon-voi-jquery-validation-XL6lAkwJKek -->
<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" type="text/css" href="./list_sv.css">

</head>


<body id="body">
    <?php
    require_once "database.php";
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if ($_POST["button"] == "confirm") {
            $sql = "INSERT INTO SINHVIEN (HoVaTenSV, GioiTinh, MaKH, NgaySinh, DiaChi, HinhAnh)
    VALUES (?, ?, ?, ?, ?, ?);";
            if ($stmt = $conn->prepare($sql)) {
                $stmt->bind_param(
                    "ssssss",
                    $_POST["uname"],
                    $_POST["gen"],
                    $_POST["depart"],
                    $_POST["d"],
                    $_POST["add"],
                    $_POST["file_address"]
                );    // "s" string type      
                $stmt->execute();
    ?>
                <form id="form_temp" action="" method="post">

                </form>
                <script type="text/javascript">
                    document.getElementById('form_temp').submit();
                </script>


    <?php
            }
        }
    }
    ?>

    <div class="container">

        <?php
        $gender = array(0 => "Nam", 1 => "Nữ");
        $department = array(
            "nothing" => "Chọn phân khoa", "MAT" => "Khoa học máy tính",
            "KDL" => "Khoa học vật liệu"
        );
        ?>

        <form>
            <fieldset class="group_department">
                <label id="department" for="depart" style="padding-right: 35px;padding-left: 2px;">Khoa</label>
                <select id="select_depart" name="depart">
                    <?php
                    foreach ($department as $key => $value) :
                        if ($key == "nothing") :
                            echo '<option selected="selected" value="' . $key . '">' . $value . '</option>';
                            continue;
                        else :
                            echo $key;
                            echo '<option value="' . $key . '">' . $value . '</option>'; //close your tags!!
                        endif;
                    endforeach;
                    ?>
                </select>
            </fieldset>

            <fieldset class="key_word">
                <label id="key_word_label" for="key_w">Từ khóa</label>
                <input id="key_word_input" type="text" name="key_w">
            </fieldset>

            <fieldset class="search_button">
                <button id="my_search_button" name="search_but" type="submit">Tìm kiếm</button>
            </fieldset>
        </form>

        <fieldset>
            <form action="register.php" method="post">
                <p id="number_student">Số sinh viên tìm thấy: XXX</p>
                <button id="my_insert_button" name="insert_but" type="submit">Thêm</button>
            </form>
        </fieldset>


        <fieldset>
            <?php

            $sql = "Select ID, HoVaTenSV, MaKH FROM SINHVIEN";
            $result = $conn->query($sql);
            ?>

            <table>
                <tr>
                    <th>No</th>
                    <th>Tên sinh viên</th>
                    <th align="left">Khoa</th>
                    <th class="action">Action</th>
                </tr>
                <?php
                if ($result->num_rows > 0) {
                    $index = 0;
                    while ($data = $result->fetch_assoc()) {
                        $index++;
                ?>
                        <tr>

                            <td><?php echo $index; ?> </td>
                            <td><?php echo $data['HoVaTenSV']; ?> </td>
                            <td style="width: 65%; text-align:left;"><?php echo $data['MaKH']; ?> </td>
                            <td class="action" style="width: 15%;">
                                <button class="delete_change">Xóa</button>
                                <button class="delete_change">Sửa</button>
                            </td>
                        <tr>
                        <?php

                    }
                } else { ?>
                        <tr>
                            <td>No data found</td>
                        </tr>
                    <?php } ?>
            </table>
        </fieldset>
    </div>

</body>

</html>